package android.kuva.com.threadtest;

import android.database.Observable;
import android.os.AsyncTask;

/**
 * Created by KuVa on 29.07.2015.
 */
public class WorkModel
{
    private boolean isWorking;
    private final WorkObservable observable = new WorkObservable();
    private FactTask factTask;

    public void countFact(final int number)
    {
        if (isWorking)
            return;

        observable.notifyStarted();
        isWorking = true;
        factTask = new FactTask(number);
        factTask.execute();
    }

    public void stopCountFact()
    {
        if (isWorking)
            factTask.cancel(true);
        isWorking = false;
    }

    public void registerObserver (final Observer observer)
    {
        observable.registerObserver(observer);
        if (isWorking)
            observer.onWorkStarted();
    }

    public void unregisterObserver (final Observer observer)
    {
        observable.unregisterObserver(observer);
    }

    private class FactTask extends AsyncTask<Void, Void, Integer>
    {
        private int nubmer;

        public FactTask(final int nubmer)
        {
            this.nubmer = nubmer;
        }

        @Override
        protected Integer doInBackground(Void... params)
        {
            try
            {
                Thread.sleep(3000);
            } catch (InterruptedException e)
            {
                e.printStackTrace();
            }
            return fact(nubmer);

        }

        @Override
        protected void onPostExecute(Integer result)
        {
            isWorking = false;

            if (result != null)
            {
                observable.notifySucceeded(result);
            }
        }

        private int fact(int x)
        {
            if (x == 0)
                return 1;
            else
                return x * fact(x-1);
        }
    }



    private class WorkObservable extends Observable<Observer>
    {
        public void notifyStarted()
        {
            for (final Observer observer : mObservers)
            {
                observer.onWorkStarted();
            }
        }

        public void notifySucceeded(int result)
        {
            for (final Observer observer :mObservers)
            {
                observer.onWorkSucceeded(result);
            }
        }

          public void notifyFailed()
        {
            for (final Observer observer : mObservers)
            {
                observer.onWorkFailed();
            }
        }
    }
}
