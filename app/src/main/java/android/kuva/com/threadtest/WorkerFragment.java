package android.kuva.com.threadtest;

import android.app.Fragment;
import android.os.AsyncTask;
import android.os.Bundle;
import android.widget.TextView;


/**
 * Created by KuVa on 29.07.2015.
 */
public class WorkerFragment extends Fragment
{
    WorkModel workModel;

    public WorkerFragment()
    {
        workModel = new WorkModel();
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    public WorkModel getWorkModel ()
    {
        return workModel;
    }



}
