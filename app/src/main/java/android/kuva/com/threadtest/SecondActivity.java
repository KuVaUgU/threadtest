package android.kuva.com.threadtest;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;


/**
 * Created by KuVa on 29.07.2015.
 */
public class SecondActivity extends Activity implements Observer
{
    private static final String TAG = "TAG_WORKER";

    private EditText editText;
    private Button okButton;
    private TextView textView;
    private ProgressBar progressBar;

    private WorkModel workModel;

    public static void startActivity(Context context)
    {
        Intent intent = new Intent(context, SecondActivity.class);
        context.startActivity(intent);
    }
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        editText = (EditText) findViewById(R.id.second_editText);
        okButton = (Button) findViewById(R.id.second_okButton);
        textView = (TextView) findViewById(R.id.second_textView);
        progressBar = (ProgressBar) findViewById(R.id.second_progressBar);
        showProgress(false);

        okButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                String s = editText.getText().toString();
                if (s.isEmpty())
                {
                    Toast.makeText(SecondActivity.this, "Enter an integer" , Toast.LENGTH_LONG).show();
                    textView.setText("");
                }
                else
                {
                    workModel.countFact(Integer.parseInt(s));
                    textView.setText("");
                }

            }
        });

        final WorkerFragment retainedWorkerFragment = (WorkerFragment) getFragmentManager().findFragmentByTag(TAG);

        if (retainedWorkerFragment != null)
        {
            workModel = retainedWorkerFragment.getWorkModel();
        }
        else
        {
            final WorkerFragment workerFragment = new WorkerFragment();

            getFragmentManager().beginTransaction()
                    .add(workerFragment, TAG)
                    .commit();

            workModel = workerFragment.getWorkModel();
        }

        workModel.registerObserver(this);
    }

    @Override
    protected void onDestroy()
    {
        super.onDestroy();
        workModel.unregisterObserver(this);

        if (isFinishing())
        {
            workModel.stopCountFact();
        }
    }

    @Override
    public void onWorkStarted()
    {
        showProgress(true);
    }

    @Override
    public void onWorkSucceeded(int result)
    {
        textView.setText(String.valueOf(result));
        showProgress(false);
    }

    @Override
    public void onWorkFailed()
    {
        showProgress(false);
        Toast.makeText(this, "FAILED", Toast.LENGTH_SHORT).show();

    }

    private void showProgress(final boolean show)
    {
        editText.setEnabled(!show);
        okButton.setEnabled(!show);
        progressBar.setVisibility(show ? View.VISIBLE : View.GONE);
    }
}
