package android.kuva.com.threadtest;

/**
 * Created by KuVa on 30.07.2015.
 */
public interface Observer {
    void onWorkStarted();

    void onWorkSucceeded(int result);

    void onWorkFailed();
}